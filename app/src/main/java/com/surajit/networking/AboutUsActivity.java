package com.surajit.networking;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

public class AboutUsActivity extends ActionBarActivity{

	
	int installedVersionCode;
	String installedVersionName;
	TextView textVersion;
	private ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// Creating view correspoding to the fragment
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aboutus);
		
		//textVersion = findViewById(R.id.textView2);
		PackageInfo pinfo;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			installedVersionCode = pinfo.versionCode; // 105 
	        installedVersionName = pinfo.versionName; // 1.0.5
	        
	        //textVersion.setText(installedVersionName);
	        
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background));
	}	
	
}
