package com.surajit.networking;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.surajit.networking.socket.CommandClientCallback;
import com.surajit.networking.socket.CommandClient;
import com.surajit.networking.utility.NetworkUtility;
import com.surajit.networking.utility.Utility;

public class ClientCameraActivity extends ActionBarActivity implements OnLongClickListener, OnClickListener, CommandClientCallback {
	
	private EditText editTextMsg;
	private Button buttonSend;
	private CommandClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_NO_TITLE); 
		//getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		ActionBar actionBar = getSupportActionBar();
		if(actionBar!=null) {
			///actionBar.hide();
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background));
		}
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //turn on screen
		
		setContentView(R.layout.activity_client_camera);

		buttonSend = (Button) findViewById(R.id.buttonSend);
		buttonSend.setOnClickListener(this);
		editTextMsg = (EditText) findViewById(R.id.editTextMsg);

		client = new CommandClient(getServerIp(),Data.commandPort,this);

	}

	private String getServerIp(){
		String str="";

		String ip = NetworkUtility.getWifiIp(this);
		int index = ip.lastIndexOf('.');
		if (index != -1) {
			str = ip.substring(0, index);
			str = str + ".1";
		}

		return str;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.client_camera, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onLongClick(View v) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if(id == R.id.buttonSend){
			client.send(editTextMsg.getText().toString());
		}
	}

	@Override
	public void onCommandFinished(boolean success, String output) {
		if(success){
			Toast.makeText(this, "Message sent", Toast.LENGTH_SHORT).show();
			Utility.logInfo("Received message: "+output);
		} else{
			Toast.makeText(this, "Error...", Toast.LENGTH_SHORT).show();
		}
	}
}
