package com.surajit.networking;

import java.io.File;

public class Data {
	public static int commandPort = 38345;
	public static File rootDir;
	public static String rootDirName = "WifiCamera";
	public static String CAPTURE = "CPTR";
	public static String AUTOFOCUS = "ATOFCS";
	public static String OK = "OKAY";
	public static String FAIL = "FAIL";
	public static String ECHO = "ECHO";
}
