package com.surajit.networking;

import java.io.File;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements OnClickListener {
	
	private Button buttonCameraService;
	private Button buttonCamera;
	private final int CAMERA_CALL_BACK_RESULT = 31;
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		buttonCameraService = (Button)findViewById(R.id.buttonCameraService);
		buttonCamera = (Button)findViewById(R.id.buttonCamera);
		
		buttonCameraService.setOnClickListener(this);
		buttonCamera.setOnClickListener(this);
		if(Data.rootDir == null){
			//Data.rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
			//Data.rootDir.mkdirs();
			try{
				Data.rootDir = new File(Environment.getExternalStorageDirectory(),Data.rootDirName);
				Data.rootDir.mkdirs();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background));
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_aboutus) {
			//Intent intent = new Intent(this,AboutUsActivity.class);
			//startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if(id == R.id.buttonCameraService){
			Intent intent = new Intent(this,ServerCameraActivity.class);
			startActivity(intent);
		}
		else if(id == R.id.buttonCamera){
			Intent intent = new Intent(this,WifiListActivity.class);
			startActivity(intent);
		}
	}
    
    
}
