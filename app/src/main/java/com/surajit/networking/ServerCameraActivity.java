package com.surajit.networking;

import com.surajit.networking.socket.CommandServer;
import com.surajit.networking.socket.CommandServerCallback;
import com.surajit.networking.utility.PromptDialog;
import com.surajit.networking.wifi.WifiUtility;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ServerCameraActivity extends ActionBarActivity implements OnClickListener, CommandServerCallback {
	private Button buttonCamera;
	private String startText = "Start Service";
	private String stopText = "Stop Service";
	private WifiUtility wifi;
	private Handler mHandler;
	private ActionBar actionBar;
	private ListView listViewOutput;
	private ArrayAdapter<String> adapter;
	private List<String> outputList;
	private CommandServer server;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //turn on screen

		setContentView(R.layout.activity_server_camera);
		this.mHandler = new Handler();
		buttonCamera = (Button) findViewById(R.id.buttonStartCamera);
		buttonCamera.setOnClickListener(this);
		buttonCamera.setText(startText);
		listViewOutput = (ListView) findViewById(R.id.listViewOutput);
		
		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background));
		
		wifi = new WifiUtility(this);

		outputList = new ArrayList<>();
		adapter = new ArrayAdapter<>(this,R.layout.server_list_output,android.R.id.text1,outputList);
		listViewOutput.setAdapter(adapter);

		server = new CommandServer(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.server_camera, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			new PromptDialog(this,"Settings","Enter port number"){

				@Override
				public boolean onOkClicked(String input) {
					boolean ret = false;
					try{
						Data.commandPort = Integer.valueOf(input);
						ret = true;
					} catch (NumberFormatException e){
						Toast.makeText(getApplicationContext(), "Port must be number", Toast.LENGTH_SHORT).show();
						ret = false;
					}
					return ret;
				}

			}.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		server.stopServer();
		if(wifi.stopHotspot()){
			finish();
		}
	}  

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.buttonStartCamera){
			String buttonText = buttonCamera.getText().toString();
			if(buttonText.equalsIgnoreCase(startText)){
				buttonCamera.setEnabled(false);
				new StartHotspot(this).execute();
			}
			else if(buttonText.equalsIgnoreCase(stopText)){
				onBackPressed();
			}
			else{
				Toast.makeText(this, "Wrong Command", Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void displayMessage(final String msg){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				outputList.add(msg);
				adapter.notifyDataSetChanged();
				listViewOutput.post(new Runnable() {
					@Override
					public void run() {
						// Select the last row so it will scroll into view...
						listViewOutput.setSelection(adapter.getCount() - 1);
					}
				});
			}
		});
	}

	@Override
	public void onCommandReceived(String command) {
		displayMessage(command);
	}

	@Override
	public void onServerStarted(int port) {
		displayMessage("Server started on port : "+port);
	}

	@Override
	public void onServerStopped() {
		onBackPressed();
	}

	private class StartHotspot extends AsyncTask<Void,Void,Boolean>{

		private ProgressDialog pDialog;
		private Context context;
		
		StartHotspot(Context context){
			this.context = context;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(context);
            pDialog.setMessage("Setting Wifi Hotspot. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
			buttonCamera.setEnabled(false);
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(wifi.isHotspotEnabled())
				return true;

			if(wifi.isWifiEnabled()){
				wifi.stopWifi();
			}
			boolean ret = wifi.startHotspot();
			//boolean ret = wifi.startHotspot("WifiCamera","commonburro",true);
			if(ret){
				while(!wifi.isHotspotEnabled()){
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			buttonCamera.setEnabled(true);
			if(result){
				displayMessage("Hotspot SSID : "+wifi.getHotspotSSID());
				String pswd = wifi.getHotspotPassword();
				if(pswd == null)
					pswd = "";
				displayMessage("Hotspot Password : "+pswd);
				if(startServer()) {
					buttonCamera.setText(stopText);
					Toast.makeText(context, "Service Started...", Toast.LENGTH_SHORT).show();
				} else{
					Toast.makeText(context, "Service Start Error...", Toast.LENGTH_SHORT).show();
				}
			}
			else{
				Toast.makeText(context, "Hotspot Start Error..", Toast.LENGTH_SHORT).show();
			}
		}

		private boolean startServer(){
			boolean ret;
			try{
				server.startServer();
				ret = true;
			} catch (Exception e){
				e.printStackTrace();
				ret = false;
			}
			return ret;
		}
		
	}
}
