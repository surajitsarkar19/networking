package com.surajit.networking;

import java.util.ArrayList;
import java.util.List;

import com.surajit.networking.utility.NetworkUtility;
import com.surajit.networking.utility.PromptDialog;
import com.surajit.networking.wifi.WifiItem;
import com.surajit.networking.wifi.WifiListAdapter;
import com.surajit.networking.wifi.WifiStateReceiver;
import com.surajit.networking.wifi.WifiUtility;
import com.surajit.networking.wifi.WifiStateReceiver.WifiStateListener;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class WifiListActivity extends ActionBarActivity implements WifiStateListener, OnClickListener {
	
	WifiReceiver receiverWifi;
	WifiUtility wifi;
	ArrayList<WifiItem> connections;
	WifiListAdapter adapter;
	ListView listView;
	WifiStateReceiver wifiStateReceiver;
	private BroadcastReceiver mReceiver;
	private WifiItem  wifiItem;
	private ActionBar actionBar;
	private int selectedItem;
	private Button buttonScan ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifi_list);
		
		receiverWifi = new WifiReceiver();
		wifiStateReceiver = new WifiStateReceiver(this);
		registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		registerReceiver(wifiStateReceiver, new IntentFilter(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION));
		//registerWifiReceiver();
		wifi = new WifiUtility(this);
		connections = new ArrayList<WifiItem>();
		listView = (ListView) findViewById(R.id.listViewNetwork);
		adapter = new WifiListAdapter(this,R.layout.wifi_item_layout, connections);
		buttonScan = (Button) findViewById(R.id.buttonScanWifi);
		buttonScan.setOnClickListener(this);
		// Assign adapter to ListView
		listView.setAdapter(adapter);
		//Toast.makeText(this,""+ NetworkUtility.isWifiEnabled(this), 0).show();
		//Toast.makeText(this,""+ wifi.isWifiEnabled(), 0).show();
		if(!wifi.isWifiEnabled()){
			wifi.startWifi();
		}
		else{
			//for first time only
			List<ScanResult> wifiList = wifi.getScanResults();
			loadWifiData(wifiList);
		}
		
		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background));
       
		
		wifiItem = null;
		listView.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			selectedItem = position;

            // ListView Clicked item value
			wifiItem    = (WifiItem) listView.getItemAtPosition(position);
		
			WifiInfo info = wifi.getConnectionInfo();
			String ssid = info.getSSID();
			
			if(ssid!=null){
				String itemssid = wifi.convertToQuotedString(wifiItem.getSsid());
				if( NetworkUtility.isWifiEnabled(WifiListActivity.this) && (ssid.equals(wifiItem.getSsid()) || ssid.equals(itemssid))){
					//MessageDialog.show(WifiListActivity.this,"Wifi Settings", "This device is already connected to this network");
					//Toast.makeText(WifiListActivity.this, "This device is already connected to this network", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(WifiListActivity.this,ClientCameraActivity.class);
					startActivity(intent);
					finish();
				}
				else{
					if(wifiItem.getSecurityLevel() == WifiUtility.SECURITY_NONE){
						//new ConnectHotspot(WifiListActivity.this).execute(Integer.toString(wifiItem.getSecurityLevel()), wifiItem.getSsid(),"");
						boolean ret = wifi.connect(wifiItem.getSecurityLevel(), wifiItem.getSsid(),"");
					}
					else{
						new PromptDialog(WifiListActivity.this,"Wifi Settings","Enter wifi password"){
	
							@Override
							public boolean onOkClicked(String input) {
								// TODO Auto-generated method stub
								boolean ret = wifi.connect(wifiItem.getSecurityLevel(), wifiItem.getSsid(),input);
								//new ConnectHotspot(WifiListActivity.this).execute(Integer.toString(wifiItem.getSecurityLevel()), wifiItem.getSsid(),input);
								return ret;
							}
							
						}.show();
					}
				}
			}
			else{
				if(wifiItem.getSecurityLevel() == WifiUtility.SECURITY_NONE){
					//new ConnectHotspot(WifiListActivity.this).execute(Integer.toString(wifiItem.getSecurityLevel()), wifiItem.getSsid(),"");
					boolean ret = wifi.connect(wifiItem.getSecurityLevel(), wifiItem.getSsid(),"");
				}
				else{
					new PromptDialog(WifiListActivity.this,"Wifi Settings","Enter wifi password"){

						@Override
						public boolean onOkClicked(String input) {
							// TODO Auto-generated method stub
							boolean ret = wifi.connect(wifiItem.getSecurityLevel(), wifiItem.getSsid(),input);
							//new ConnectHotspot(WifiListActivity.this).execute(Integer.toString(wifiItem.getSecurityLevel()), wifiItem.getSsid(),input);
							return true;
						}
						
					}.show();
				}
			}
			
		}
      });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.wifi_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onPause()
    {
        unregisterReceiver(receiverWifi);
        unregisterReceiver(wifiStateReceiver);
		//unregisterWifiReceiver();
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        registerReceiver(wifiStateReceiver, new IntentFilter(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION));
    	//registerWifiReceiver();
        super.onResume();
    }

	private synchronized void loadWifiData(List<ScanResult> wifiList){
    	ArrayList<WifiItem> conn=new ArrayList<WifiItem>();
    	WifiInfo connectionInfo=null;
    	if(NetworkUtility.isWifiEnabled(this)){
    		connectionInfo = wifi.getConnectionInfo();
    	}
    	for(int i = 0; i < wifiList.size(); i++)
        {
        	ScanResult result = wifiList.get(i);
        	WifiItem item = new WifiItem(result.SSID,result.BSSID,result.level,result.frequency,wifi.getSecurity(result));
        	if(connectionInfo!=null){
        		String ssid = connectionInfo.getSSID();
        		//if(ssid.equals(result.SSID) || ssid.equals(wifi.convertToQuotedString(result.SSID))){
        		if(wifi.compareSSID(ssid, result.SSID)){
            		item.setConnectionStatus(true);
            	}
        	}
        	
        	conn.add(item);
        }
        connections.clear();
        connections.addAll(conn);
        adapter.notifyDataSetChanged();
    }
	
    class WifiReceiver extends BroadcastReceiver
    {
        public void onReceive(Context c, Intent intent)
        {
        	String action  = intent.getAction();
        	if(action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)){
	        	try{
		            List<ScanResult> wifiList=null;
		            wifiList = wifi.getScanResults();
		            if(wifiList!=null){            	
		            	loadWifiData(wifiList);
		            }
	        	}
	        	catch(Exception e){}
        	}
        }
    }

	@Override
	public void authenticationError() {
		// TODO Auto-generated method stub
		/*if(wifiItem!=null){
			new PromptDialog(WifiListActivity.this,"Wifi Setting","Authentication failed.. Please provide proper password"){
	
				@Override
				public boolean onOkClicked(String input) {
					// TODO Auto-generated method stub
					boolean ret = wifi.connect(wifiItem.getSecurityLevel(), wifiItem.getSsid(),input);
					return true;
				}
				
			}.show();
		}*/
		Toast.makeText(this, "Authentication Failed...", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void authenticating() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Authenticating..", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void authenticationSuccess() {
		// TODO Auto-generated method stub
		if(wifi!=null){
			WifiInfo info = wifi.getConnectionInfo();
			Toast.makeText(this, "Connected to "+info.getSSID(), Toast.LENGTH_SHORT).show();
			while(!NetworkUtility.isWifiEnabled(this)){
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			loadWifiData(wifi.getScanResults());
			
			if(wifi.compareSSID(wifiItem.getSsid(), info.getSSID())){
				/*while(!NetworkUtility.isWifiEnabled(this)){
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
				Intent intent = new Intent(this,ClientCameraActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}

	@Override
	public void disconnected() {
		// TODO Auto-generated method stub
		List<ScanResult> wifiList=null;
        wifiList = wifi.getScanResults();
        if(wifiList!=null){            	
        	loadWifiData(wifiList);
        }
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.buttonScanWifi){
			new ScanHotspot(this).execute();
		}
	}
	
	private class ScanHotspot extends AsyncTask<Void,Void,Void>{
		
		private ProgressDialog pDialog;
		private Context context;
		private List<ScanResult> wifiList;

		ScanHotspot(Context context){
			this.context = context;
			this.wifiList = null;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(context);
            pDialog.setMessage("Scanning wifi. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(wifi!=null){
				wifi.restartWifi();
		        wifiList = wifi.getScanResults();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();		
			if(wifiList!=null){            	
	        	loadWifiData(wifiList);
	        }
		}
	}
	
	/*public void registerWifiReceiver(){
		IntentFilter mFilter = new IntentFilter();
	    mFilter.addAction(WifiUtility.WIFI_STATE_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.SCAN_RESULTS_AVAILABLE_ACTION);
	    mFilter.addAction(WifiUtility.NETWORK_IDS_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.SUPPLICANT_STATE_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.CONFIGURED_NETWORKS_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.LINK_CONFIGURATION_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.NETWORK_STATE_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.RSSI_CHANGED_ACTION);
	    mFilter.addAction(WifiUtility.ERROR_ACTION);
	
	    mReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            //handleWifiEvent(context, intent);
	        }
	    };
	    registerReceiver(mReceiver,mFilter);
	}
	public void unregisterWifiReceiver(){
		unregisterReceiver(mReceiver);
	}*/
	
	/*private class ConnectHotspot extends AsyncTask<String,Void,Boolean>{

		private ProgressDialog pDialog;
		private Context context;
		private int security;
		private String ssid;
		private String passwd;
		
		ConnectHotspot(Context context){
			this.context = context;
			security = -1;
			ssid = null;
			passwd = null;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(context);
            pDialog.setMessage("Connectinggg. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			security = Integer.parseInt(params[0]);
			ssid = params[1];
			passwd = params[2];
			
			boolean ret = wifi.connect(security, ssid,passwd);
			if(ret){
				while(!NetworkUtility.isWifiEnabled(context)){
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			Intent intent = new Intent(WifiListActivity.this,ClientCameraActivity.class);
			startActivity(intent);
			//finish();
			
		}
		
		
	}*/
	
}
