package com.surajit.networking.socket;

import android.os.Handler;
import android.os.Looper;

import com.surajit.networking.Data;
import com.surajit.networking.utility.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Surajit on 12/10/2017.
 */

public class CommandClient implements Runnable{
    private DatagramSocket socket;
    private String cmd;
    private CommandClientCallback callback;
    private boolean running;
    private String ip;
    private int port;
    private Thread thread;
    private Handler handler;
    public CommandClient(String ip, int port,CommandClientCallback callback){
        this.socket = null;
        this.callback = callback;
        this.ip = ip;
        this.port = port;
        setRunning(false);
        handler = new Handler(Looper.getMainLooper());
    }
    private boolean openSocket(){
        boolean ret = false;
        try{
            //socket = new Socket();
            //socket.bind(null);
            //socket.setSoTimeout(SOCKET_TIMEOUT);
            //socket.connect(new InetSocketAddress(ip, port), SOCKET_TIMEOUT);
            socket = new DatagramSocket();
            ret = true;
        }
        catch(Exception e){ret=false;e.printStackTrace();}
        return ret;
    }

    private void closeSocket(){
        if(socket!=null){
            try {
                socket.close();
                socket = null;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    public void send(String msg){
        if(!isRunning()){
            thread = new Thread(this,"CommandThread");
            this.cmd = msg;
            thread.start();
        }
    }

    private synchronized boolean isRunning(){
        return this.running;
    }

    private synchronized void setRunning(boolean statue){
        this.running = statue;
    }

    public void run() {
        Utility.logInfo("Client thread started");
        setRunning(true);
        boolean bCommandStatus = false;
        String commandResponse = "";
        try{
            if(openSocket()){
                byte[] sendData = cmd.getBytes();
                DatagramPacket packet = new DatagramPacket(sendData, sendData.length,InetAddress.getByName(ip),Data.commandPort);
                socket.send(packet);

                byte [] receiveData = new byte[1024];
                packet = new DatagramPacket(receiveData, receiveData.length);
                socket.receive(packet);
                commandResponse = new String(packet.getData(), 0, packet.getLength());
                bCommandStatus = true;
            }
        }
        catch(Exception e){
            bCommandStatus = false;
            e.printStackTrace();
        }
        finally{
            closeSocket();
        }
        sendCallback(bCommandStatus,commandResponse);
        setRunning(false);
        Utility.logInfo("Client thread stopped");
    }

    private void sendCallback(final boolean status, final String msg){
        if(callback!=null){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onCommandFinished(status,msg);
                }
            });
        }
    }
}
