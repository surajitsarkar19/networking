package com.surajit.networking.socket;

/**
 * Created by Surajit on 12/10/2017.
 */

public interface CommandClientCallback {
    void onCommandFinished(boolean success,String output);
}
