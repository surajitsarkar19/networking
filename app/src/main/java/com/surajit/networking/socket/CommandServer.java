package com.surajit.networking.socket;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.surajit.networking.Data;
import com.surajit.networking.utility.Utility;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;

/**
 * Created by surajit on 12/3/17.
 */

public class CommandServer implements Runnable{
    private boolean stopThread;
    private DatagramSocket commandServer = null;
    private Thread thread;
    private CommandServerCallback callback;

    public CommandServer(CommandServerCallback callback){
        stopThread = true;
        this.callback = callback;
    }

    public void startServer() throws IOException {
        if(stopThread) {
            Utility.logInfo("Starting server");
            thread = new Thread(this);
            thread.setName("Command Server");
            commandServer = new DatagramSocket(Data.commandPort);
            stopThread = false;
            thread.start();
        }else{
            Utility.logInfo("Server already running");
        }
    }

    public void stopServer(){
        if(!stopThread && commandServer!=null){
            Utility.logInfo("Stopping server");
            stopThread = true;
            commandServer.close();
        } else{
            Utility.logInfo("Server already stopped");
        }
    }

    public void run(){
        Utility.logInfo( "Server thread started");
        callback.onServerStarted(commandServer.getLocalPort());
        while(!stopThread)
        {
            try
            {
                byte[] receiveData = new byte[1024];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                commandServer.receive(receivePacket);

                String sentence = new String( receivePacket.getData(), Charset.defaultCharset());
                System.out.println("RECEIVED: " + sentence);
                callback.onCommandReceived(sentence);

                if (sentence.equals("end")) {
                    //server end condition
                    stopThread = true;
                    continue;
                }

                InetAddress IPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();
                String capitalizedSentence = sentence.toUpperCase();
                byte[] sendData = capitalizedSentence.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                commandServer.send(sendPacket);


            } catch(Exception e){
                stopThread = true;
                e.printStackTrace();
                break;
            }
        }
        commandServer = null;
        Utility.logInfo( "Server thread closed");
        callback.onServerStopped();
    }
}
