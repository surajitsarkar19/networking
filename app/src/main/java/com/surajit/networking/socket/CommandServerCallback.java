package com.surajit.networking.socket;

/**
 * Created by Surajit on 12/10/2017.
 */

public interface CommandServerCallback {
    void onCommandReceived(String command);
    void onServerStarted(int port);
    void onServerStopped();
}
