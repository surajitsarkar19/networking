package com.surajit.networking.utility;

import com.surajit.networking.R;

import android.app.AlertDialog;  
import android.content.Context;  
import android.content.DialogInterface;  
import android.content.DialogInterface.OnClickListener;  

public abstract class ConfirmDialog extends AlertDialog.Builder implements OnClickListener{

	public ConfirmDialog(Context context, String title, String message) {
		super(context);
		// TODO Auto-generated constructor stub
		setTitle(title);  
		setMessage(message);  
		setIcon(R.drawable.ic_launcher);
		setPositiveButton("OK", this);
		setNegativeButton("Cancel", this);
	}
	
	public ConfirmDialog(Context context,String message) {
		super(context);
		// TODO Auto-generated constructor stub
		String title = Utility.getApplicationName(context);
		setTitle(title);  
		setMessage(message);  
		setIcon(R.drawable.ic_launcher);
		setPositiveButton("OK", this);
		setNegativeButton("Cancel", this);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		if (which == DialogInterface.BUTTON_POSITIVE) {  
			 OnOK(dialog);
		} 
		else {  
			OnCancel(dialog); 
		} 
	}
	
	abstract public void OnOK(DialogInterface dialog);
	abstract public void OnCancel(DialogInterface dialog);

}
