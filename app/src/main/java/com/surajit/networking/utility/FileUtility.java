package com.surajit.networking.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class FileUtility {
	
	private static String TAG = "FileUtility";

	public FileUtility() {
		// TODO Auto-generated constructor stub
	}
	
	public static String getFileName(String path){
		String name = null;
		if(path == null)
			return null;
		int index = path.lastIndexOf(File.separatorChar);
		if(index!=-1)
			name = path.substring(index+1);
		return name;
	}
	
	public static boolean isSDCardMounted(){
		String mediaState=Environment.getExternalStorageState();
		boolean ret = false;
		if(mediaState.equals(Environment.MEDIA_MOUNTED)){
			ret = true;
		}
		return ret;
	}
	
	public static File getRootDir(Context context,String dirName){
		/*File f1=Environment.getExternalStorageDirectory(); // /mnt/sdcard
	    File f2=Environment.getDataDirectory(); // /data
	    File f3=context.getExternalFilesDir("ss"); // /mnt/sdcard/com.android.sstandroid/files/ss
	    File f4=context.getFilesDir(); // /data/data/com.android.sstandroid/files
	    File f5=Environment.getExternalStoragePublicDirectory("bb"); // /mnt/sdcard/bb
	    File f6=Environment.getRootDirectory(); // /system
	    File f7=context.getDir("sss", context.MODE_PRIVATE); // /data/data/com.dextrosoft.sstandroid/app_sss
	    File f8=context.getCacheDir(); // /data/data/com.dextrosoft.sstandroid/cache
	    File f9=context.getDatabasePath("mmssms.db"); // /data/data/com.dextrosoft.sstandroid/databases/mmssms.db
	    */
		File f1=null;
		if(isSDCardMounted()){
			f1=new File(Environment.getExternalStorageDirectory(),dirName);
			if(f1.exists()){
				Log.d(TAG,"Project root Directory found.");
			}
			else{
				Log.d(TAG,"Creating new directory root directory");
				f1.mkdir();
			}
		}
		else{
			Log.d(TAG,"External Media is not mounted. Using Internal space");
			f1=context.getDir(dirName,Context.MODE_PRIVATE);
		}
		Log.i(TAG,"Root Directory :"+f1.getPath());
		return f1;
		
	}
	
	public static boolean streamCopy(File source, File dest) throws IOException {
		boolean success=false;
		InputStream input = null;
		OutputStream output = null;
		try {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
			success=true;
		} 
		catch(Exception e){
			success=false;
			e.printStackTrace();
		}finally {
			input.close();
			output.close();
		}
		return success;
	}

	public static boolean channelCopy(File source, File dest) throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		boolean success=false;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
			success=true;
		} 
		catch(Exception e){
			success=false;
			e.printStackTrace();
		}finally {
			inputChannel.close();
			outputChannel.close();
		}
		return success;
	}
	
	public static boolean move(File source, File dest) throws IOException{
		boolean success = false;
		success = channelCopy(source,dest);
		success = source.delete();
		return success;
	}
	
	public static void deleteAllFiles(File dir, boolean dirFlag){
		/*
		 * dir = root directory
		 * dirFlag = true ---->Delete all nested directories and files
		 * dirFlag = false---->Delete only nested files. Do not delete any nested directory
		 */
		File[] files = dir.listFiles();
		
		//delete all files
		for(File f1 : files){
			if(f1.isDirectory()){
				if(f1.list().length == 0){
					//empty directory
					if(dirFlag)
						f1.delete();
				}
				else{
					deleteAllFiles(f1,dirFlag);
					//recheck if the directory is empty or not
					if(f1.list().length == 0){
						//empty
						if(dirFlag)
							f1.delete();
					}
				}
			}
			else{
				f1.delete();
			}
		}
		
	}
	
	/**
	  Remove a directory and all of its contents.

	  The results of executing File.delete() on a File object
	  that represents a directory seems to be platform
	  dependent. This method removes the directory
	  and all of its contents.

	  @return true if the complete directory was removed, false if it could not be.
	  If false is returned then some of the files in the directory may have been removed.

	*/
	public static boolean removeDirectory(File directory) {

	  // System.out.println("removeDirectory " + directory);

	  if (directory == null)
	    return false;
	  if (!directory.exists())
	    return true;
	  if (!directory.isDirectory())
	    return false;

	  String[] list = directory.list();

	  // Some JVMs return null for File.list() when the
	  // directory is empty.
	  if (list != null) {
	    for (int i = 0; i < list.length; i++) {
	      File entry = new File(directory, list[i]);

	      //        System.out.println("\tremoving entry " + entry);

	      if (entry.isDirectory())
	      {
	        if (!removeDirectory(entry))
	          return false;
	      }
	      else
	      {
	        if (!entry.delete())
	          return false;
	      }
	    }
	  }

	  return directory.delete();
	}

}
