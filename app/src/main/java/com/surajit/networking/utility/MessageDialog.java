package com.surajit.networking.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.surajit.networking.R;

public abstract class MessageDialog extends AlertDialog.Builder implements OnClickListener{

	public MessageDialog(Context context, String title, String message) {
		super(context);
		// TODO Auto-generated constructor stub
		setTitle(title);  
		setMessage(message);  
		setIcon(R.drawable.ic_launcher);
		setPositiveButton("OK", this);
		setCancelable(false);
	}
	
	public MessageDialog(Context context,String message) {
		super(context);
		// TODO Auto-generated constructor stub
		String title = Utility.getApplicationName(context);
		setTitle(title);  
		setMessage(message);  
		setIcon(R.drawable.ic_launcher);
		setPositiveButton("OK", this);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		if (which == DialogInterface.BUTTON_POSITIVE) {  
			 if(OnOK()){
				 dialog.dismiss();
			 }
		}
	}
	abstract public boolean OnOK();
}
