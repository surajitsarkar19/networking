package com.surajit.networking.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

public class Utility {

	private static String TAG = "Networking";

	public static void logDebug(String msg){
		Log.d(TAG,msg);
	}

	public static void logInfo(String msg){
		Log.i(TAG,msg);
	}

	public static void logError(String msg, Throwable e){
		Log.e(TAG,msg,e);
	}
	
	public static String getVersion(Context paramContext){
	    try
	    {
	      String str = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
	      return str;
	    }
	    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
	    return null;
	 }
	
	public static String getApplicationName(Context paramContext){
	    try
	    {
	      ApplicationInfo ai = paramContext.getPackageManager()
	    		  .getApplicationInfo(paramContext.getPackageName(), 0);
	      String str = (String)paramContext.getPackageManager().getApplicationLabel(ai);
	      return str;
	    }
	    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
	    return null;
	 }
	
	public static int getVersionCode(Context context){
		int version = -1;
	    try {
	        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
	        version = pInfo.versionCode;
	    } catch (NameNotFoundException e1) {logError("Package not found", e1);
	    }
	    return version;
	}
	
	public static String getVersionName(Context context,String packageName){
		String version = null;
	    try {
	        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
	        version = pInfo.versionName;
	    } catch (NameNotFoundException e1) {
	        logError("Package "+packageName+" not found", e1);
	    }
	    return version;
	}
	
	public static String getDate(long milliSeconds) {
	    //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(milliSeconds);
	    if(milliSeconds==0)
	    	return "";
	    else
	    	return formatter.format(calendar.getTime());
	}
	
	public static String getDatetime(long milliSeconds) {
	    //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(milliSeconds);
	    if(milliSeconds==0)
	    	return "";
	    else
	    	return formatter.format(calendar.getTime());
	}
	
	public static String getDate(Date date) {
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
	    return formatter.format(date);
	}
	
	public static long getDateInMillisec(){
		Date date = new Date();
		long dt = date.getTime();
		return dt;
	}
	
	public static String exec(String command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
            // Reads stdout.
            // NOTE: You can write to stdin of the command using process.getOutputStream().
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            int read;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((read = reader.read(buffer)) > 0) {
                output.append(buffer, 0, read);
            }
            reader.close();
            process.waitFor();
            return output.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
	
	public static void runCommnad(String cmd){
		try {

	        Process p = Runtime.getRuntime().exec( "su" );
	        InputStream es = p.getErrorStream();
	        DataOutputStream os = new DataOutputStream(p.getOutputStream());

	        os.writeBytes(cmd + "\n");

	        os.writeBytes("exit\n");
	        os.flush();

	        int read;
	        byte[] buffer = new byte[4096];
	        String output = new String();
	        while ((read = es.read(buffer)) > 0) {
	            output += new String(buffer, 0, read);
	        }

	        p.waitFor();

	    } 
		catch (IOException e) {
	        Log.e(TAG, e.getMessage());
	        e.printStackTrace();
	    }
		catch (InterruptedException e) {
	        Log.v(TAG, e.getMessage());
	        e.printStackTrace();
	    }
	}
	
	public static boolean isFloat(String str){
		// get the count of .
		int dotCount = getCharCount(str,'.');
		String reg = "^[-+]?[0-9]*\\.?[0-9]+$";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(str);
		if(m.matches()){
			
			if(dotCount==0 || dotCount >1){
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return false;
		}
	}
	
	public static boolean isInteger(String str){
		String reg = "^[-+]?[0-9]+$";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(str);
		if(m.matches()){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static int getCharCount(String str,char ch){
		int count=0;
		int length = str.length();
		int i=0;
		while(i < length){
			char c = str.charAt(i);
			if(c == ch){
				count++;
			}
		}
		return count;
	}
	
	public static boolean isEmpty(String str){
		boolean ret = false;
		if(str == null){
			ret = true;
		}
		else if(str.length() == 0){
			ret = true;
		}
		else{
			ret = false;
		}
		return ret;
	}
	
	public static void showKeyBoard(Context context){
		InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null){
			imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
		}
	}
	
	public static void hideKeyBoard(Context context){
		InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null){
			imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
		}
	}
	
	/*
	 * to get two decimal point output pass extent value 2
	 */
	public static double parseDouble(double value, int extent){ 
		double base = Math.pow(10, extent);
		double bal = value*base;
		bal = Math.round(bal);
		bal = bal/base;
		return bal;
	}
	
	public static String getDeviceName() {
		  String manufacturer = Build.MANUFACTURER;
		  String model = Build.MODEL;
		  if (model.startsWith(manufacturer)) {
		    return capitalize(model);
		  } else {
		    return capitalize(manufacturer) + " " + model;
		  }
		}


	private static String capitalize(String s) {
		  if (s == null || s.length() == 0) {
		    return "";
		  }
		  char first = s.charAt(0);
		  if (Character.isUpperCase(first)) {
		    return s;
		  } else {
		    return Character.toUpperCase(first) + s.substring(1);
		  }
	} 
	
	public static String getOSVersion(){
		String myVersion = android.os.Build.VERSION.RELEASE; // e.g. myVersion := "1.6"
		int sdkVersion = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8; 
		return myVersion;
	}
	
	public static String getFileName(String prefix,String extension){
		String ret = "";
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("ddMMyyHHmmss");
		ret = DATE_FORMAT.format(new Date());
		if(prefix!=null){
			ret = prefix+"_"+ret;
		}
		if(extension.startsWith(".")){
			ret+=extension;
		}
		else{
			ret +="."+extension;
		}
		return ret;
	}
	
}


