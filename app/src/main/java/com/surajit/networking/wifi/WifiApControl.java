package com.surajit.networking.wifi;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
 
public class WifiApControl {
    private static Method getWifiApState = null;
    private static Method isWifiApEnabled = null;
    private static Method setWifiApEnabled = null;
    private static Method getWifiApConfiguration = null;
    private static Method setWifiApConfiguration = null;
    private static Method setWifiApConfig = null; //for htc device.
 
    public static final String WIFI_AP_STATE_CHANGED_ACTION = "android.net.wifi.WIFI_AP_STATE_CHANGED";
 
    public static final int WIFI_AP_STATE_DISABLED = WifiManager.WIFI_STATE_DISABLED;
    public static final int WIFI_AP_STATE_DISABLING = WifiManager.WIFI_STATE_DISABLING;
    public static final int WIFI_AP_STATE_ENABLED = WifiManager.WIFI_STATE_ENABLED;
    public static final int WIFI_AP_STATE_ENABLING = WifiManager.WIFI_STATE_ENABLING;
    public static final int WIFI_AP_STATE_FAILED = WifiManager.WIFI_STATE_UNKNOWN;
 
    public static final String EXTRA_PREVIOUS_WIFI_AP_STATE = WifiManager.EXTRA_PREVIOUS_WIFI_STATE;
    public static final String EXTRA_WIFI_AP_STATE = WifiManager.EXTRA_WIFI_STATE;
 
    static {
        // lookup methods and fields not defined publicly in the SDK.
        Class<?> cls = WifiManager.class;
        for (Method method : cls.getDeclaredMethods()) {
            String methodName = method.getName();
            if (methodName.equals("getWifiApState")) {
                getWifiApState = method;
            } else if (methodName.equals("isWifiApEnabled")) {
                isWifiApEnabled = method;
            } else if (methodName.equals("setWifiApEnabled")) {
                setWifiApEnabled = method;
            } else if (methodName.equals("getWifiApConfiguration")) {
                getWifiApConfiguration = method;
            }else if(methodName.equals("setWifiApConfiguration")){
            	setWifiApConfiguration = method;
            }else if(methodName.equals("setWifiApConfig")){
            	setWifiApConfig = method;
            }
        }
    }
    
    public static boolean isHtc(){ //HTC use seperate class to store wifi hotspot configuration
    	boolean ret = false;
    	try {  
            Field field = WifiConfiguration.class.getDeclaredField("mWifiApProfile");  
            ret = field != null;  
        } catch (Exception e) {  
        } 
    	return ret;
    }
 
    public static boolean isApSupported() {
        return (getWifiApState != null && isWifiApEnabled != null
                && setWifiApEnabled != null && getWifiApConfiguration != null);
    }
 
    private WifiManager mgr;
 
    private WifiApControl(WifiManager mgr) {
        this.mgr = mgr;
    }
 
    /**
     * Initiates Wifi Access Point Control.
     * Use 
     * {@code WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);}
     * to get WifiManager object.
     * @param mgr WifiManager instance
     * @return WifiApControl instance
     */
    public static WifiApControl getApControl(WifiManager mgr) {
    	if(mgr==null)
    		return null;
        if (!isApSupported())
            return null;
        return new WifiApControl(mgr);
    }
 
    /**
     * Return whether Wi-Fi AP is enabled or disabled.
     * @return {@code true} if Wi-Fi AP is enabled
     * @see #getWifiApState()
     *
     * @hide Dont open yet
     */
    public boolean isWifiApEnabled() {
        try {
            return (Boolean) isWifiApEnabled.invoke(mgr);
        } catch (Exception e) {
            Log.v("BatPhone", e.toString(), e); // shouldn't happen
            return false;
        }
    }
 
    /**
     * Gets the Wi-Fi enabled state.
     * @return One of {@link #WIFI_AP_STATE_DISABLED},
     *         {@link #WIFI_AP_STATE_DISABLING}, {@link #WIFI_AP_STATE_ENABLED},
     *         {@link #WIFI_AP_STATE_ENABLING}, {@link #WIFI_AP_STATE_FAILED}
     * @see #isWifiApEnabled()
     *
     * @hide Dont open yet
     */
    public int getWifiApState() {
        try {
            return (Integer) getWifiApState.invoke(mgr);
        } catch (Exception e) {
            Log.v("BatPhone", e.toString(), e); // shouldn't happen
            return -1;
        }
    }
 
    /**
     * Gets the Wi-Fi AP Configuration.
     * @return AP details in WifiConfiguration
     *
     * @hide Dont open yet
     */
    public WifiConfiguration getWifiApConfiguration() {
        try {
            return (WifiConfiguration) getWifiApConfiguration.invoke(mgr);
        } catch (Exception e) {
            Log.v("BatPhone", e.toString(), e); // shouldn't happen
            return null;
        }
    }
    
    /**
     * Sets the Wi-Fi AP Configuration.
     * @return {@code true} if the operation succeeded, {@code false} otherwise
     *
     * @hide Dont open yet
     */
    public boolean setWifiApConfiguration(WifiConfiguration wifiConfig) {
        try {
        	return (Boolean) setWifiApConfiguration.invoke(mgr,wifiConfig);
        } catch (Exception e) {
            return false;
        }
    }
    
    //for HTC device
    public boolean setWifiApConfig(WifiConfiguration wifiConfig) {
        try {
        	int i = (Integer) setWifiApConfiguration.invoke(mgr,wifiConfig);
        	boolean ret = i>0;
        	return ret;
        } catch (Exception e) {
            return false;
        }
    }
 
    /**
     * Start AccessPoint mode with the specified
     * configuration. If the radio is already running in
     * AP mode, update the new configuration
     * Note that starting in access point mode disables station
     * mode operation
     * @param config SSID, security and channel details as
     *        part of WifiConfiguration
     * @return {@code true} if the operation succeeds, {@code false} otherwise
     *
     * @hide Dont open up yet
     */
    public boolean setWifiApEnabled(WifiConfiguration config, boolean enabled) {
        try {
            return (Boolean) setWifiApEnabled.invoke(mgr, config, enabled);
        } catch (Exception e) {
            Log.e("BatPhone", e.toString(), e); // shouldn't happen
            return false;
        }
    }

}
