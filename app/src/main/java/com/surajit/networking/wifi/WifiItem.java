package com.surajit.networking.wifi;

import android.net.wifi.WifiManager;

public class WifiItem {
	
	private String ssid;
	private String bssid;
	private int level;
	private int frequency;
	private int security;
	private boolean connected;
	public WifiItem(String ssid, String bssid, int level, int frequency, int security) {
		this.ssid = ssid;
		this.bssid = bssid;
		this.level = level;
		this.frequency = frequency;
		this.security = security;
		this.connected = false;
	}
	public String getSsid() {
		return ssid;
	}
	public String getBssid() {
		return bssid;
	}
	public int getSignalStrength() {
		return WifiManager.calculateSignalLevel(level,4)+1;
	}
	public int getFrequency() {
		return frequency;
	}
	public String getSecurity(){
		String str = null;
		switch(security){
		case WifiUtility.SECURITY_NONE :str = "Open Network";break;
		case WifiUtility.SECURITY_PSK :str = "WPA/WPA2 PSK";break;
		case WifiUtility.SECURITY_WEP :str = "WEP";break;
		case WifiUtility.SECURITY_EAP :str = "EAP";break;
		default :str = "Unknown Security Type";break;
		}
		return str;
	}
	public int getSecurityLevel(){
		return this.security;
	}
	public String toString(){
		return this.ssid;
	}
	
	public void setConnectionStatus(boolean status){
		this.connected=status;
	}
	
	public boolean isConnected(){
		return this.connected;
	}
	
}
