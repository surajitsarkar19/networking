package com.surajit.networking.wifi;

import java.util.List;

import com.surajit.networking.R;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WifiListAdapter extends ArrayAdapter<WifiItem>{
	
	private int resource;
	private int connectionColor = 0xFF44B1C7;
	private int normalColor = 0xFFFFFFFF;

	public WifiListAdapter(Context context, int resource, List<WifiItem> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.resource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		WifiItem item = getItem(position);
		if (convertView == null) {
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
	    	LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
	    	convertView = vi.inflate(resource, parent, false);
	    	
	    	ImageView imgView = (ImageView)convertView.findViewById(R.id.imageViewSignalStrength);
			TextView ssidView = (TextView)convertView.findViewById(R.id.textViewSSID);
			TextView secView = (TextView)convertView.findViewById(R.id.textViewEncryption);
			
			holder = new ViewHolder();
			holder.ssid = ssidView;
			holder.security = secView;
			holder.signal = imgView;
			
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.ssid.setText(item.getSsid());
		if(item.isConnected()){
			holder.ssid.setTextColor(connectionColor);
		}
		else{
			holder.ssid.setTextColor(normalColor);
		}
		holder.security.setText(item.getSecurity());
		
		int signal = item.getSignalStrength();
		if(signal<0)
			signal=0;
		if(signal == 0){
			holder.signal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.wifi_signal_0));
		}
		else if(signal==1){
			holder.signal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.wifi_signal_1));
		}
		else if(signal==2){
			holder.signal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.wifi_signal_2));
		}
		else if(signal==3){
			holder.signal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.wifi_signal_3));
		}
		else{
			holder.signal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.wifi_signal_4));
		}
		
		return convertView;
	}
	
	private static class ViewHolder{
		TextView ssid;
		TextView security;
		ImageView signal;
	}

}
