package com.surajit.networking.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * 
 * @author Dextrosoft
 * 
 * To get the receiver working, the following intentfilter need to be added
 * 
 * IntentFilter mIntentFilter = new IntentFilter();
 * mIntentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
 * registerReceiver(receiverWifi, mIntentFilter);
 *
 */

public class WifiStateReceiver extends BroadcastReceiver
{
	public interface WifiStateListener{
		void authenticationError();
		void authenticating();
		void authenticationSuccess();
		void disconnected();
	}
	
	WifiStateListener listener;
	public WifiStateReceiver(WifiStateListener listener){
		this.listener=listener;
	}
    public void onReceive(Context c, Intent intent)
    {
    	try{
	    	String action  = intent.getAction();
	        if(action.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)){
	            Log.d("WifiReceiver", ">>>>SUPPLICANT_STATE_CHANGED_ACTION<<<<<<");
	            SupplicantState supl_state=((SupplicantState)intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE));
	            switch(supl_state){
	            case ASSOCIATED:
	            	Log.i("SupplicantState", "ASSOCIATED");
	                break;
	            case ASSOCIATING:
	            	Log.i("SupplicantState", "ASSOCIATING");
	                break;
	            case AUTHENTICATING:
	            	listener.authenticating();
	            	Log.i("SupplicantState", "Authenticating...");
	                break;
	            case COMPLETED:
	            	listener.authenticationSuccess();
	            	Log.i("SupplicantState", "Connected");
	                break;
	            case DISCONNECTED:
	            	listener.disconnected();
	            	Log.i("SupplicantState", "Disconnected");
	                break;
	            case DORMANT:
	            	Log.i("SupplicantState", "DORMANT");
	                break;
	            case FOUR_WAY_HANDSHAKE:
	            	Log.i("SupplicantState", "FOUR_WAY_HANDSHAKE");
	                break;
	            case GROUP_HANDSHAKE:
	            	Log.i("SupplicantState", "GROUP_HANDSHAKE");
	                break;
	            case INACTIVE:
	            	Log.i("SupplicantState", "INACTIVE");
	                break;
	            case INTERFACE_DISABLED:
	            	Log.i("SupplicantState", "INTERFACE_DISABLED");
	                break;
	            case INVALID:
	            	Log.i("SupplicantState", "INVALID");
	                break;
	            case SCANNING:
	            	Log.i("SupplicantState", "SCANNING");
	                break;
	            case UNINITIALIZED:
	            	Log.i("SupplicantState", "UNINITIALIZED");
	                break;
	            default:
	            	Log.i("SupplicantState", "Unknown");
	                break;
	
	            }
	            int supl_error=intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, -1);
	            if(supl_error==WifiManager.ERROR_AUTHENTICATING){
	                //Log.i("ERROR_AUTHENTICATING", "ERROR_AUTHENTICATING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	            	//Toast.makeText(c, "!!! Authentication Error !!!", 0).show();
	            	listener.authenticationError();
	            }
	        }
    	}catch(Exception e){e.printStackTrace();}
    }
}
