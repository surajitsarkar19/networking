package com.surajit.networking.wifi;

import java.lang.reflect.Field;
import java.util.List;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;



public class WifiUtility {

	private Context context;
	private WifiManager wifiManager;
	private WifiConfiguration configuration;
	private WifiConfiguration oldConfiguration;
	private boolean mIsHtc;
	private WifiApControl apControl;
	
	public static final int SECURITY_NONE = 0;
    public static final int SECURITY_WEP = 1;
    public static final int SECURITY_PSK = 2;
    public static final int SECURITY_EAP = 3;
    
    public static final String WIFI_STATE_CHANGED_ACTION = WifiManager.WIFI_STATE_CHANGED_ACTION;
    public static final String SCAN_RESULTS_AVAILABLE_ACTION = WifiManager.SCAN_RESULTS_AVAILABLE_ACTION;
    public static final String NETWORK_IDS_CHANGED_ACTION = WifiManager.NETWORK_IDS_CHANGED_ACTION;
    public static final String SUPPLICANT_STATE_CHANGED_ACTION = WifiManager.SUPPLICANT_STATE_CHANGED_ACTION;
    public static final String CONFIGURED_NETWORKS_CHANGED_ACTION = "android.net.wifi.CONFIGURED_NETWORKS_CHANGE";;
    public static final String LINK_CONFIGURATION_CHANGED_ACTION = "android.net.wifi.LINK_CONFIGURATION_CHANGED";
    public static final String NETWORK_STATE_CHANGED_ACTION = WifiManager.NETWORK_STATE_CHANGED_ACTION;
    public static final String RSSI_CHANGED_ACTION = WifiManager.RSSI_CHANGED_ACTION;
    public static final String ERROR_ACTION = "android.net.wifi.ERROR";
	
	public WifiUtility(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		configuration = null;
		oldConfiguration = null;
		mIsHtc = WifiApControl.isHtc();
		apControl = WifiApControl.getApControl(wifiManager);
	}
	
	public boolean startHotspot(){
		boolean ret = false;
		if(wifiManager!=null){
			apControl = WifiApControl.getApControl(wifiManager);
	        if (apControl != null) { 
	            // TURN OFF YOUR WIFI BEFORE ENABLE HOTSPOT
	            //if (isWifiOn(context) && isTurnToOn) {
	            //  turnOnOffWifi(context, false);
	            //}
	 
	            ret = apControl.setWifiApEnabled(apControl.getWifiApConfiguration(),true);
	        }
		}
		return ret;
	}

	public String getHotspotSSID(){
		String ssid = "";
		WifiConfiguration config = apControl.getWifiApConfiguration();
		ssid = config.SSID;
		return ssid;
	}

	public String getHotspotPassword(){
		String ssid = "";
		WifiConfiguration config = apControl.getWifiApConfiguration();
		ssid = config.preSharedKey;
		return ssid;
	}
	
	public boolean startHotspot(String ssid,String password,boolean retainConfiguration){
		//set Wpa2 protocol
		boolean ret = false;
		WifiApControl apControl = WifiApControl.getApControl(wifiManager);
		oldConfiguration = null;
		configuration = apControl.getWifiApConfiguration();
		if(retainConfiguration)
			oldConfiguration = apControl.getWifiApConfiguration();
		setConfiguration(ssid,password);
		ret = apControl.setWifiApEnabled(configuration,true);
		return ret;
	}

	private void setConfiguration(String ssid, String password){
		if(mIsHtc){
			setConfigurationHTC(configuration,ssid,password);
		}
		else{
			setConfigurationOther(configuration,ssid,password);
		}
	}
	
	private void setConfigurationOther(WifiConfiguration config, String ssid, String password){
		config.SSID = ssid;
		config.preSharedKey  = password;
		/*config.status = WifiConfiguration.Status.ENABLED;
		config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
		config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
		config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
		config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
		config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
		config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);*/


		config.hiddenSSID = false;
		config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
		config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
		config.allowedKeyManagement.set(4);
		config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
		config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	}
	
	private  boolean setConfigurationHTC (WifiConfiguration apconfig, String ssid, String password) {
        boolean successed = true ;
       
//        WifiConfiguration mnetConfig = new WifiConfiguration (); 
       Field localField1;

       try {

    	   localField1  = WifiConfiguration.class .getDeclaredField ( "mWifiApProfile" );
		   localField1.setAccessible ( true );
		   Object localObject2 = localField1.get (apconfig);
		   localField1.setAccessible ( false );
		    if (localObject2 != null ) {
		           
		    		// set SSID 
		           Field localField5 = localObject2.getClass().getDeclaredField ("SSID" );
		           localField5.setAccessible ( true );
		           localField5.set (localObject2, ssid);
		           localField5.setAccessible ( false );
		           
		
		           // Set the security type 
		           Field localField2 = localObject2.getClass().getDeclaredField ( "secureType" );
		           localField2.setAccessible ( true );
		           //localField2.set (localObject2, "Open" );
		           localField2.set (localObject2, "wpa-psk" );
		           localField2.setAccessible ( false );
		
		           // Password Settings 
		           Field localField3 = localObject2.getClass().getDeclaredField ( "key" );
		           localField3.setAccessible ( true );
		           localField3.set (localObject2, password);
		           localField3.setAccessible ( false );
		           
		           /*
		           // IP address settings 
		           Field localField4 = localObject2.getClass().getDeclaredField ("ipAddress" );
		           localField4.setAccessible ( true );
		           localField4.set (localObject2, "192.168.1.1" );
		           localField4.setAccessible ( false );
		
		           // subnet mask setting 
		           Field localField6 = localObject2.getClass().getDeclaredField ("DhcpSubnetMask");
		           localField6.setAccessible ( true );
		           localField6.set (localObject2, "255.255.255.0" );
		           localField6.setAccessible ( false );
		           
		           // Start IP settings 
		           Field localField8 = localObject2.getClass().getDeclaredField ( "startingIP" );
		           localField8.setAccessible ( true );
		           localField8.set (localObject2, "192.168.1.100" );
		           localField8.setAccessible ( false );
		
		           // DHCP settings 
		           Field localField7 = localObject2.getClass().getDeclaredField ( "dhcpEnable" );
		           localField7.setAccessible ( true );
		           localField7.set (localObject2, 1 );
		           localField7.setAccessible ( false );
		           */
		    }

       } catch (Exception e) {

           e.printStackTrace ();

       }
       return successed;
   }

	public boolean stopHotspot(){
		boolean ret = false;
		if(wifiManager!=null){
	        if (apControl != null) {

	            ret = apControl.setWifiApEnabled(apControl.getWifiApConfiguration(),false);
	        }
		}
		return ret;
	}
	
	/*public boolean stopHotspot(){
		boolean ret = false;
		if(wifiManager!=null){
	        if (apControl != null) {
				ret = apControl.setWifiApEnabled(apControl.getWifiApConfiguration(),false);
				if(oldConfiguration!=null) {
					if (mIsHtc) {
						ret &= apControl.setWifiApConfig(oldConfiguration);
					} else {
						ret &= apControl.setWifiApConfiguration(oldConfiguration);
					}
				}
	            
	        }
		}
		ret &= wifiManager.saveConfiguration();
		return ret;
	}*/
	
	public boolean isHotspotEnabled(){
		return apControl.isWifiApEnabled();
	}
	
	public boolean startWifi(){
		boolean ret = false;
		if(wifiManager!=null){
			ret = wifiManager.setWifiEnabled(true);
		}
		return ret;
	}
	
	public boolean stopWifi(){
		boolean ret = false;
		if(wifiManager!=null){
			ret = wifiManager.setWifiEnabled(false);
		}
		return ret;
	}
	
	public void restartWifi(){
		stopWifi();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		startWifi();
		while(!isWifiEnabled()){
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public List<ScanResult> getScanResults(){
		List<ScanResult> scanResult = null;
		if(wifiManager!=null){
			scanResult = wifiManager.getScanResults();
		}
		return scanResult;
	}
	
	public boolean isWifiEnabled(){
		if(wifiManager!=null){
			return wifiManager.isWifiEnabled();
		}
		else{
			return false;
		}
	}
	
	/**
	 * Compares if two given SSIDs are equal or not.
	 * SSID can be both quoted and unquoted based on based device.
	 * This method will compare two ssids irrespectively if they are quoted or not.
	 * @param arg0
	 * @param arg1
	 * @return on success return true, otherwise false
	 */
	public boolean compareSSID(String arg0, String arg1){
		boolean ret = false;
		boolean flag0 = false, flag1=false;
		flag0 = isQuotedString(arg0);
		flag1 = isQuotedString(arg1);
		if(flag0&&flag1){
			//both are quoted
			ret = arg0.equals(arg1);
		}
		else if(flag0){
			//first one is quoted string
			ret = arg0.equals(convertToQuotedString(arg1));
		}
		else if(flag1){
			//second one is quoted string
			ret = arg1.equals(convertToQuotedString(arg0));
		}
		else{
			//both are unquoted
			ret = arg0.equals(arg1);
		}
		return ret;
	}
	
	private boolean isQuotedString(String str){
		int firstIndex = -1;
		int lastIndex = -1;
		int len = str.length();
		firstIndex = str.indexOf('"');
		lastIndex = str.lastIndexOf('"');
		if(firstIndex == 0 && lastIndex == len-1){
			return true;
		}
		else{
			return false;
		}
	}
	
	public int getSecurity(WifiConfiguration config) {
	    if (config.allowedKeyManagement.get(KeyMgmt.WPA_PSK)) {
	        return SECURITY_PSK;
	    }
	    if (config.allowedKeyManagement.get(KeyMgmt.WPA_EAP) ||
	            config.allowedKeyManagement.get(KeyMgmt.IEEE8021X)) {
	        return SECURITY_EAP;
	    }
	    return (config.wepKeys[0] != null) ? SECURITY_WEP : SECURITY_NONE;
	}

	public int getSecurity(ScanResult result) {
	    if (result.capabilities.contains("WEP")) {
	        return SECURITY_WEP;
	    } else if (result.capabilities.contains("PSK")) {
	        return SECURITY_PSK;
	    } else if (result.capabilities.contains("EAP")) {
	        return SECURITY_EAP;
	    }
	    return SECURITY_NONE;
	}
	
	public String removeDoubleQuotes(String string) {
	int length = string.length();
    if ((length > 1) && (string.charAt(0) == '"')
                && (string.charAt(length - 1) == '"')) {
           return string.substring(1, length - 1);
        }
       return string;
    }
		
    public String convertToQuotedString(String string) {
    	if(string==null)
    		return null;
    	else
    		return "\"" + string + "\"";
    }
    
    public WifiInfo getConnectionInfo(){
    	if(wifiManager!=null){
    		return wifiManager.getConnectionInfo();
    	}
    	else{
    		return null;
    	}
    }
    
    public boolean connect(int security,String ssid,String password){
    	boolean ret=false;
    	if(wifiManager!=null){
    		WifiConfiguration wfc = new WifiConfiguration();;
    		//stopWifi();
    		ssid = convertToQuotedString(ssid);
    		password = convertToQuotedString(password);
    		switch(security){
    		case SECURITY_NONE :
    			wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
    			wfc.allowedAuthAlgorithms.clear();
    			wfc.status=WifiConfiguration.Status.ENABLED;
    			wfc.SSID = ssid;
    			break;
    		
    		case SECURITY_WEP :
    			wfc.status=WifiConfiguration.Status.ENABLED;
    			wfc.priority = 40;
    		    wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
    		    wfc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
    		    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
    		    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
    		    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
    		    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
			    wfc.wepKeys[0]= password;
    			wfc.wepTxKeyIndex = 0;
    			wfc.SSID = ssid;
    			break;
    			
    		case SECURITY_PSK :
    			wfc.status = WifiConfiguration.Status.ENABLED;
    			wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
    			wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
    			wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
    			//wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
    			wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
    			wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
    			wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
    			wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
    			wfc.SSID = ssid;
    			wfc.preSharedKey = password;
    			break;
    			
    			default :return false;
    		}
    		
    		int netId = wifiManager.addNetwork(wfc);
    		System.out.println(wfc.toString());
    		if(netId!=-1){
    			//success
    			wifiManager.saveConfiguration();
    			//ret = wifiManager.enableNetwork(netId, true);
    		}

			List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
			for( WifiConfiguration i : list ) {
				if(i.SSID != null && i.SSID.equals(ssid)) {
					wifiManager.disconnect();
					wifiManager.enableNetwork(i.networkId, true);
					wifiManager.reconnect();
					ret = true;
					break;
				}
			}
    	}
    	return ret;
    }

}
